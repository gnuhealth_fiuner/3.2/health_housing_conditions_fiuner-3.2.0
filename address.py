# -*- coding: utf-8 -*-
##############################################################################

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

 
__all__ = ['Address']


class Address(metaclass = PoolMeta):
    "Address"
    __name__ = 'party.address'
  
    @staticmethod
    def default_city():
        Config = Pool().get('gnuhealth.configuration')
        config = Config (1)
        if config.address_city_default:
            return config.address_city_default
        return None

    @staticmethod
    def default_zip():
        Config = Pool().get('gnuhealth.configuration')
        config = Config (1)
        if config.address_zip_default:
            return config.address_zip_default
        return None

    @staticmethod
    def default_subdivision():
        Config = Pool().get('gnuhealth.configuration')
        config = Config (1)
        if config.address_subdivision_default:
            return config.address_subdivision_default.id
        return None

    @staticmethod
    def default_country():
        Config = Pool().get('gnuhealth.configuration')
        config = Config (1)
        if config.address_country_default:
            return config.address_country_default.id
        return None
  

  
