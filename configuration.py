#! -*- coding: utf-8 -*-
##############################################################################

from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import Eval, Bool


__all__ = ['GnuHealthConfiguration','GnuHealthSequences']

class GnuHealthConfiguration(ModelSingleton, ModelView, ModelSQL):
    'Domiciliary Unit and Citizenship Configuration'
    __name__ = 'gnuhealth.configuration'
    
    address_country_default = fields.Property(fields.Many2One('country.country',
                'Pais de la Unidad Domiciliaria por defecto',
                required=True))
    
    address_subdivision_default = fields.Property(fields.Many2One('country.subdivision',
                'Provincia por defecto',
                domain=[('country','=',Eval('address_country_default'))],
                depends=['address_country_default'],
                required = True))

    address_city_default = fields.Property(fields.Char('Ciudad'))
    
    address_zip_default = fields.Property(fields.Char('Codigo Postal'))
    
    citizenship_default = fields.Property(fields.Many2One('country.country',
                'Nacionalidad por defecto',
                required=True))
    
    residence_default = fields.Property(fields.Many2One('country.country',
                'Pais de residencia por defecto',
                required=True))
    
    
class GnuHealthSequences(ModelSingleton, ModelSQL, ModelView):
    __name__ = 'gnuhealth.sequences'

    housing_du_conditions_survey_sequence = fields.Property(fields.Many2One('ir.sequence',
        'Housing DU Conditions Survey Sequence', required=True,
        domain=[('code', '=', 'gnuhealth.housing')]))
